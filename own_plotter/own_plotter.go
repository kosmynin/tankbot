package own_plotter

import (
	"log"
	"math"
	"strconv"
	"strings"
	"time"

	"github.com/mazznoer/colorgrad/scheme"
	"gitlab.com/kosmynin/TankBot/database"

	"gonum.org/v1/plot"
	"gonum.org/v1/plot/font"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/vg"
	"gonum.org/v1/plot/vg/draw"

	"golang.org/x/text/cases"
    "golang.org/x/text/language"
)

// Create Statistics Graph
func PlotData(user int64) {
	// Set Font to sans-serif
	plot.DefaultFont = font.Font{
		Typeface: "Liberation",
		Variant:  "Sans",
	}
	grad := scheme.Category10
	p := plot.New()

	var yticks []plot.Tick
	var xticks []plot.Tick

	// Prepare X-Axis Ticks
	location, _ := time.LoadLocation("Europe/Berlin")

	for i := time.Now().Add(-time.Hour * 10 * 24).Round(time.Hour).Unix(); i < time.Now().Unix(); i += int64(2 * time.Hour.Seconds()) {
		xticks = append(xticks, plot.Tick{float64(i), string(i)})
	}

	timeTicks := plot.TimeTicks{
		Ticker: plot.ConstantTicks(xticks),
		Format: "Mon, 02.01 15:04 ",
		Time:   func(t float64) time.Time { return time.Unix(int64(t), 0).In(location) },
	}

	// Set Title, rotate X-Axis Labels
	p.Title.Text = "Preisverlauf"
	p.Title.TextStyle.Font.Size = 50
	p.Y.Label.Text = "Preis"
	p.Y.Label.TextStyle.Font.Size = 25
	p.Y.Tick.Label.Font.Size = 25
	p.X.Tick.Marker = timeTicks
	p.X.Tick.Label.Rotation = math.Pi * 0.3
	p.X.Tick.Label.XAlign = draw.XRight
	p.X.Tick.Label.YAlign = draw.YCenter
	p.X.Tick.Label.Font.Size = 25
	p.Legend.Top = true
	p.Legend.TextStyle.Font.Size = 40
	p.Legend.ThumbnailWidth = 10 * vg.Centimeter

	// Draw a grid behind the data
	p.Add(plotter.NewGrid())

	// Draw Line and Scatterplots
	stationsStr := database.GetSetStations(user)
	stations := strings.Split(stationsStr, ",")
	typ := database.GetType(user)
	num := 0
	
	for i, s := range stations {
		data := database.GetLastWeeks(s, typ)
		line, points, err := plotter.NewLinePoints(data)
		if err != nil {
			log.Panic(err)
		}
		line.Color = grad[i]
		points.Color = grad[i]
		points.GlyphStyle.Shape = draw.CircleGlyph{}
		points.GlyphStyle.Radius = 0.2 * vg.Centimeter
		p.Add(line)
		p.Add(points)
		
		gly := *points
		gly.GlyphStyle.Radius = 0.5 * vg.Centimeter
		p.Legend.Add(cases.Title(language.German).String(database.GetStationName(s)), &gly)
		// this is used so only the top 10 stations get plotted, it can be pushed to up to 10 with the current schema, after that we do not have any colours left
		if i >= 9 {
			break
		}
		num++
	}
	
	// Offset the title and legend
	p.Legend.YOffs = p.Legend.TextStyle.FontExtents().Height * vg.Length(num) // Adjust the legend up.
	p.Title.Padding = p.Legend.YOffs
	
	// Setup Y-Axis Ticks
	for i := p.Y.Min; i <= p.Y.Max; i += 0.01 {
		yticks = append(yticks, plot.Tick{float64(i), strconv.FormatFloat(float64(i), 'f', 3, 64)})
	}

	p.Y.Tick.Marker = plot.ConstantTicks(yticks)

	// Save the plot to a PNG file.
	if err := p.Save(150 *vg.Centimeter, (50*vg.Centimeter + p.Legend.YOffs), "stats_"+strconv.Itoa(int(user))+".png"); err != nil {
		log.Println("Error creating stats", err)
	}

}
