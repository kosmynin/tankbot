package main

import (
	"log"
	"net/http"

	"gitlab.com/kosmynin/TankBot/config"
	"gitlab.com/kosmynin/TankBot/database"
	"gitlab.com/kosmynin/TankBot/variables"
	"gitlab.com/kosmynin/TankBot/sender"
)


// Finally, the main function starts our server
func main() {

	//firstly we need to read the config -> then we need to change the code so that the everything will work as intended
	err := config.ReadFileAndUnmarshal(variables.ConfigFileLocation)
	if err != nil {
		log.Fatal("Configuration failed. Aborting program!!!", err.Error())
		return
	}
	database.Init()

	// closes the db after main returns
	defer variables.DB.Close()

	// Set up commands
	sender.SetCmd()

	// Start background process, updating the prices
	go sender.GetCurrentPrices()

	// Start Server
	log.Printf("Start Listening on Port 8080")
	err = http.ListenAndServe(":8080", http.HandlerFunc(sender.Handler))
	if err != nil {
		log.Fatal("Failed to start Server", err.Error())
	}
}
