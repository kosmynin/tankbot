package sender

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"math/rand"
	"mime/multipart"
	"net/http"
	"os"
	"slices"
	"strconv"
	"strings"
	"sync"
	"time"

	"golang.org/x/text/cases"
	"golang.org/x/text/language"

	"gitlab.com/kosmynin/TankBot/config"
	"gitlab.com/kosmynin/TankBot/database"
	"gitlab.com/kosmynin/TankBot/own_plotter"
	"gitlab.com/kosmynin/TankBot/variables"
)

// webhook response body
// https://core.telegram.org/bots/api#update
type webhookReqBody struct {
	Message struct {
		Text string `json:"text"`
		Chat struct {
			ID int64 `json:"id"`
		} `json:"chat"`
		Location struct {
			Long float64 `json:"longitude"`
			Lat  float64 `json:"latitude"`
			Live int64   `json:"live_period"`
		} `json:"location"`
	} `json:"message"`
	Edited_message struct {
		Text string `json:"text"`
		Chat struct {
			ID int64 `json:"id"`
		} `json:"chat"`
		Location struct {
			Long float64 `json:"longitude"`
			Lat  float64 `json:"latitude"`
			Live int64   `json:"live_period"`
		} `json:"location"`
	} `json:"edited_message"`
}

type RWMap struct {
	sync.RWMutex
	m map[int64]string
}

var bufferMap = make(map[int64]string)
var buffer = RWMap{m: bufferMap}

// Get is a wrapper for getting the value from the underlying map
func (r RWMap) Get(key int64) (string, bool) {
	r.RLock()
	defer r.RUnlock()
	val, exists := r.m[key]
	return val, exists
}

// Set is a wrapper for setting the value of a key in the underlying map
func (r RWMap) Set(key int64, val string) {
	r.Lock()
	defer r.Unlock()
	r.m[key] = val
}

// Set is a wrapper for deleting the key in the underlying map
func (r RWMap) Delete(key int64) {
	r.Lock()
	defer r.Unlock()
	delete(r.m, key)
}

// Setup types for custom keyboards
type button struct {
	Text     string `json:"text"`
	Location bool   `json:"request_location"`
}

type command struct {
	Command     string `json:"command"`
	Description string `json:"description"`
}

type keyboard struct {
	Keyboard [3][1]button `json:"keyboard"`
	One_time bool         `json:"one_time_keyboard"`
}

type remove_keyboard struct {
	Remove bool `json:"remove_keyboard"`
}

type location_keyboard struct {
	Keyboard [1][1]button `json:"keyboard"`
	One_time bool         `json:"one_time_keyboard"`
}

type notification_keyboard struct {
	Keyboard [2][1]button `json:"keyboard"`
	One_time bool         `json:"one_time_keyboard"`
}

// message requests
// https://core.telegram.org/bots/api#sendmessage
type sendMessageReqBody struct {
	ChatID int64    `json:"chat_id"`
	Text   string   `json:"text"`
	Format string   `json:"parse_mode"`
	Reply  keyboard `json:"reply_markup"`
}

type sendMessageReqBody_removeKeyboard struct {
	ChatID int64           `json:"chat_id"`
	Text   string          `json:"text"`
	Format string          `json:"parse_mode"`
	Reply  remove_keyboard `json:"reply_markup"`
}

type sendMessageReqBody_locationKeyboard struct {
	ChatID int64             `json:"chat_id"`
	Text   string            `json:"text"`
	Format string            `json:"parse_mode"`
	Reply  location_keyboard `json:"reply_markup"`
}

type sendMessageReqBody_notificationKeyboard struct {
	ChatID int64                 `json:"chat_id"`
	Text   string                `json:"text"`
	Format string                `json:"parse_mode"`
	Reply  notification_keyboard `json:"reply_markup"`
}

type setCommandsReqBody struct {
	Commands [5]command `json:"commands"`
}

type Notification struct {
	Name  string
	Price float64
}

// Send Watch Notifications
func SendNotifications() {
	settings := database.ReadSettings()

	for _, set := range settings {
		// skip the current user if they don't want to receive notifications
		if set.SendNotification == 0 {
			continue
		} else {
			var notification []Notification
			stations := strings.Split(set.Stations, ",")
			target := 100.0
			if set.Target <= 0 {
				for _, station := range stations {
					if station != "" {
						ntarget := database.GetLowest(station, set.Type) - set.Target
						if ntarget < target {
							target = ntarget
						}
					}
				}
			}
			for _, station := range stations {
				if station != "" {
					lastPrice := database.GetLastPrice(station, set.Type)
					open := database.GetStationStatus(station)
					if set.Target <= 0 {
						log.Println(station, lastPrice, target, set.LastNotification, (time.Now().After(time.Unix(set.LastNotification, 0).Add(60 * time.Minute))), target >= lastPrice, lastPrice != 0.0, open)
						if (time.Now().After(time.Unix(set.LastNotification, 0).Add(60 * time.Minute))) && target >= lastPrice && open && lastPrice != 0.0 {
							notification = append(notification, Notification{Name: database.GetStationName(station), Price: lastPrice})
						}
					} else {
						log.Println(station, lastPrice, set.Target, set.LastNotification, (time.Now().After(time.Unix(set.LastNotification, 0).Add(60 * time.Minute))), set.Target >= lastPrice, lastPrice != 0.0, open)
						if (time.Now().After(time.Unix(set.LastNotification, 0).Add(60 * time.Minute))) && set.Target >= lastPrice && open && lastPrice != 0.0 {
							notification = append(notification, Notification{Name: database.GetStationName(station), Price: lastPrice})
						}
					}
				}
			}

			if len(notification) != 0 {
				var msg string = "*Dein Wunschpreis wurde erreicht.*\n\n"
				for _, not := range notification {
					msg = msg + "*" + cases.Title(language.German).String(not.Name) + "*\n" + cases.Title(language.German).String(set.Type) + ": " + strconv.FormatFloat(not.Price, 'f', 3, 64) + "€\n"
					database.UpdateTime(set.Id)
				}
				msg = msg + "\n_Nutze /ziel um deinen Wunschpreis anzupassen._\n"
				SendMsg(set.Id, msg)
			}
		}
	}
	log.Println("Sent Notifications")
}

// Send message
func SendMsg(chatID int64, text string) error {
	// Create the request body struct
	reqBody := &sendMessageReqBody_removeKeyboard{
		ChatID: chatID,
		Text:   text,
		Format: "Markdown",
		Reply:  remove_keyboard{true},
	}
	// Create the JSON body from the struct
	reqBytes, err := json.Marshal(reqBody)
	if err != nil {
		return err
	}

	// Send a post request with your token
	res, err := http.Post("https://api.telegram.org/"+config.Data.Telegram_api_key+"/sendMessage", "application/json", bytes.NewBuffer(reqBytes))
	if err != nil {
		return err
	}
	if res.StatusCode != http.StatusOK {
		return errors.New("unexpected status" + res.Status)
	}

	return nil
}

// Set up Commands
func SetCmd() error {
	var cmd [5]command
	cmd[0] = command{"aktuell", "Schickt die aktuellen Preise an deinem Standort"}
	cmd[1] = command{"statistik", "Schickt den Preisverlauf der letzten Tage"}
	cmd[2] = command{"typ", "Ändere den Kraftstoffart der überwacht wird"}
	cmd[3] = command{"ziel", "Ändere den Wunschpreis"}
	cmd[4] = command{"benachrichtigung", "Benachrichtigungen ein- und ausschalten"}

	// Create the request body struct
	reqBody := &setCommandsReqBody{
		Commands: cmd,
	}
	// Create the JSON body from the struct
	reqBytes, err := json.Marshal(reqBody)
	if err != nil {
		return err
	}

	// Send a post request with your token
	res, err := http.Post("https://api.telegram.org/"+config.Data.Telegram_api_key+"/setMyCommands", "application/json", bytes.NewBuffer(reqBytes))
	if err != nil {
		return err
	}
	if res.StatusCode != http.StatusOK {
		return errors.New("unexpected status" + res.Status)
	}

	return nil
}

// Get fuel type
func SetType(chatID int64) {
	SendType(chatID, "Bitte wähle die *Kraftstoffart*, die ich überwachen soll.")

	target, exists := buffer.Get(chatID)
	for !exists {
		target, exists = buffer.Get(chatID)
	}
	if exists {
		database.SetType(chatID, strings.ToLower(target))

		SendMsg(chatID, "Du wirst für "+target+" benachrichtigt.")

		buffer.Delete(chatID)
		log.Println("Set Type")
	}
}

// Get target price
func SetTarget(chatID int64) {
	SendMsg(chatID, "Bitte gebe deinen *Wunschpreis* ein.\n\n_Du kannst mir deinen Wunschpreis in Euro z.B. 1,759 oder in Cent z.B. 175,9 schicken. Eine Einheit brauche ich nicht.\nAlternativ, gebe eine negative Zahl ein um automatisch einen Preis relativ zum niedrigsten Preis der letzten 10 Tage zu wählen. z.B. 0 für den niedrigsten Preis, -1 für einen cent über dem niedrigsten Preis usw._")

	target, exists := buffer.Get(chatID)
	for !exists {
		target, exists = buffer.Get(chatID)
	}
	if exists {
		preis, err := strconv.ParseFloat(strings.ReplaceAll(strings.TrimSpace(target), ",", "."), 64)
		if err != nil {
			log.Println(err)
		} else {
			if preis < 0 {
				database.SetTarget(chatID, preis/100)
				err = SendMsg(chatID, "Du wirst benachrichtigt sobald der Preis unter dem niedrigsten Preis der letzten 10 Tage plus "+strconv.FormatFloat(-preis, 'f', 0, 64)+" ct liegt.")
			} else if preis == 0 {
				database.SetTarget(chatID, preis/100)
				err = SendMsg(chatID, "Du wirst benachrichtigt sobald der Preis unter dem niedrigsten Preis der letzten 10 Tage liegt.")
			} else if preis >= 10 {
				database.SetTarget(chatID, preis/100)
				err = SendMsg(chatID, "Du wirst benachrichtigt sobald der Preis unter "+strconv.FormatFloat(preis, 'f', 1, 64)+" ct fällt.")
			} else {
				database.SetTarget(chatID, preis)
				err = SendMsg(chatID, "Du wirst benachrichtigt sobald der Preis unter "+strconv.FormatFloat(preis, 'f', 3, 64)+" € fällt.")
			}
			if err != nil {
				log.Println(err)
			} else {
				log.Println("Set Target")
			}
		}
		buffer.Delete(chatID)
	}
}

func UpdateNotification(chatID int64) {
	SendNotification(chatID, "Möchtest du Benachrichtigungen zulassen? (Du kannst dies jederzeit wieder ändern)")
	var err error = nil

	target, exists := buffer.Get(chatID)
	for !exists {
		target, exists = buffer.Get(chatID)
	}
	if exists {
		switch target {
		case "JA":
			database.UpdateNotification(chatID, 1)
			err = SendMsg(chatID, "Du erhältst in Zukunft Benachrichtigungen.")
		case "NEIN":
			database.UpdateNotification(chatID, 0)
			err = SendMsg(chatID, "Du erhältst in Zukunft keine Benachrichtigungen.")
		default:
			err = SendMsg(chatID, "Die Eingabe habe ich nicht verstanden.")
		}
		if err != nil {
			log.Println(err)
		} else {
			log.Println("Notification Settings updated.")
		}
		buffer.Delete(chatID)
	}
}

// send custom keyboard to allow the user to disable all notifications from the bot
func SendNotification(chatID int64, text string) error {
	var kdb [2][1]button
	kdb[0][0] = button{"JA", false}
	kdb[1][0] = button{"NEIN", false}

	reply := notification_keyboard{
		Keyboard: kdb,
		One_time: true,
	}
	// Create the request body struct
	reqBody := &sendMessageReqBody_notificationKeyboard{
		ChatID: chatID,
		Text:   text,
		Format: "Markdown",
		Reply:  reply,
	}
	// Create the JSON body from the struct
	reqBytes, err := json.Marshal(reqBody)
	if err != nil {
		return err
	}

	// Send a post request with your token
	res, err := http.Post("https://api.telegram.org/"+config.Data.Telegram_api_key+"/sendMessage", "application/json", bytes.NewBuffer(reqBytes))
	if err != nil {
		return err
	}
	if res.StatusCode != http.StatusOK {
		return errors.New("unexpected status" + res.Status)
	}

	return nil
}

// Send custom keyboard to select type
func SendType(chatID int64, text string) error {
	var kbd [3][1]button
	kbd[0][0] = button{"E5", false}
	kbd[1][0] = button{"E10", false}
	kbd[2][0] = button{"Diesel", false}

	reply := keyboard{
		Keyboard: kbd,
		One_time: true,
	}

	// Create the request body struct
	reqBody := &sendMessageReqBody{
		ChatID: chatID,
		Text:   text,
		Format: "Markdown",
		Reply:  reply,
	}
	// Create the JSON body from the struct
	reqBytes, err := json.Marshal(reqBody)
	if err != nil {
		return err
	}

	// Send a post request with your token
	res, err := http.Post("https://api.telegram.org/"+config.Data.Telegram_api_key+"/sendMessage", "application/json", bytes.NewBuffer(reqBytes))
	if err != nil {
		return err
	}
	if res.StatusCode != http.StatusOK {
		return errors.New("unexpected status" + res.Status)
	}

	return nil
}

// Send custom keyboard to get location
func SendLocation(chatID int64, text string) error {
	var kbd [1][1]button
	kbd[0][0] = button{"Standort senden", true}

	reply := location_keyboard{
		Keyboard: kbd,
		One_time: true,
	}

	// Create the request body struct
	reqBody := &sendMessageReqBody_locationKeyboard{
		ChatID: chatID,
		Text:   text,
		Format: "Markdown",
		Reply:  reply,
	}
	// Create the JSON body from the struct
	reqBytes, err := json.Marshal(reqBody)
	if err != nil {
		return err
	}

	// Send a post request with your token
	res, err := http.Post("https://api.telegram.org/"+config.Data.Telegram_api_key+"/sendMessage", "application/json", bytes.NewBuffer(reqBytes))
	if err != nil {
		return err
	}
	if res.StatusCode != http.StatusOK {
		return errors.New("unexpected status" + res.Status)
	}

	return nil
}

// Send Statistics
func SendStats(chatID int64) error {
	own_plotter.PlotData(chatID)

	buf := new(bytes.Buffer)
	bw := multipart.NewWriter(buf) // body writer

	f, err := os.Open("stats_" + strconv.Itoa(int(chatID)) + ".png")
	if err != nil {
		return err
	}
	defer f.Close()

	// text part1
	bw.WriteField("chat_id", fmt.Sprintf("%d", chatID))

	// file part1
	file, _ := bw.CreateFormFile("photo", "stats_"+strconv.Itoa(int(chatID))+".png")
	io.Copy(file, f)

	bw.Close() //write the tail boundry

	req, err := http.NewRequest("POST", "https://api.telegram.org/"+config.Data.Telegram_api_key+"/sendPhoto", buf)
	if err != nil {
		return err
	}
	// add headers
	req.Header.Add("Content-Type", bw.FormDataContentType())

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("request send error:", err)
		return err
	}
	resp.Body.Close()
	log.Println("Sent Stats")
	return nil
}

// Send the current prices
func SendCurrent(chatID int64) {
	stationsStr := database.GetSetStations(chatID)
	stations := strings.Split(stationsStr, ",")
	var msg string

	for _, s := range stations {
		if s != "" {
			if database.GetStationStatus(s) {
				e5 := strconv.FormatFloat(database.GetLastPrice(s, "e5"), 'f', 3, 64)
				e10 := strconv.FormatFloat(database.GetLastPrice(s, "e10"), 'f', 3, 64)
				diesel := strconv.FormatFloat(database.GetLastPrice(s, "diesel"), 'f', 3, 64)

				if e5 == "0.000" {
					e5 = "Nicht Verfügbar"
				} else {
					e5 += " €"
				}
				if e10 == "0.000" {
					e10 = "Nicht Verfügbar"
				} else {
					e10 += " €"
				}
				if diesel == "0.000" {
					diesel = "Nicht Verfügbar"
				} else {
					diesel += " €"
				}

				msg = msg + "*" + cases.Title(language.German).String(database.GetStationName(s)) + "*\nE5:        " + e5 + "\nE10:      " + e10 + "\nDiesel:  " + diesel + "\n"
			} else {
				msg = msg + "*" + cases.Title(language.German).String(database.GetStationName(s)) + "*\nGeschlossen\n"
			}
		}
	}

	SendMsg(chatID, msg)
	log.Println("Sent Current prices")
}

// Startup msg
func Start(chatID int64) {
	SendMsg(chatID, "*Hallo, ich bin der Tankpreis Bot.*\nSchicke mir einen *Standort* damit ich die Preise an diesem Standort für dich überwachen kann.\nWenn du mich nach dem aktuelle Preis an dem letzten Standort fragen willst, schicke mir */aktuell*.\nUm den Verlauf der letzten Tage zu bekommen frag mich nach der */statistik*.\n*Kleiner Tipp:* Unten links hast du einen Menü Knopf der dir alle Kommandos zeigt die ich verstehe.")

	database.WriteConfig(chatID, "e5", 0.0, time.Now().Add(time.Duration(-2)*time.Hour).Unix(), "")

	err := SendMsg(chatID, "Bevor ich anfangen kann die Preise für dich zu überwachen habe ich ein paar Fragen:")
	log.Println(err)
	SetType(chatID)
	SetTarget(chatID)
	SendLocation(chatID, "Alles was jetzt noch fehlt, damit ich an die Arbeit gehen kann, ist dein Standort.")

	// SendMsg(chatID, "Danke, ich habe deine Präferenzen gespeichert.")
	log.Println("Ran Start")
}

// Update prices
func GetCurrentPrices() {
	for {
		stations := database.GetStations()
		// splitting the original array into subarrays that are max 10 items long
		var subarrays [][]variables.Station
		for i := 0; i < len(stations); i += 10 {
			end := i + 10
			// we need this check so if the i + 10 is over the length of the array we set the end of the array as end, otherwise we get an out of bounds error
			if end > len(stations) {
				end = len(stations)
			}
			// here we split the initial array into subarrays and add those to out outer array stations[inclusive:exclusive]
			subarray := stations[i:end]
			subarrays = append(subarrays, subarray)
		}

		// iterating over the subarrays that ae max 10 items long
		for _, sub := range subarrays {
			var stationsList []string
			for _, s := range sub {
				stationsList = append(stationsList, s.Id)
			}
			stationsStr := strings.Join(stationsList[:], ",")
			requestString := fmt.Sprintf("https://creativecommons.tankerkoenig.de/json/prices.php?ids=%s&apikey=%s", stationsStr, config.Data.Tank_api)
			//panic(requestString)
			resp, err := http.Get(requestString)
			if err != nil {
				// send error to admin
				log.Println(err)
			} else if resp.StatusCode != http.StatusOK {
				log.Println(errors.New("unexpected status" + resp.Status))
			} else {
				var res variables.ApiStructTwo

				json.NewDecoder(resp.Body).Decode(&res)
				resp.Body.Close()

				for _, s := range stationsList {
					status := "closed"
					station, ok := res.Prices[s].(map[string]any)
					if ok {
						status = station["status"].(string)
					}

					e5 := database.GetLastPrice(s, "e5")
					e10 := database.GetLastPrice(s, "e10")
					diesel := database.GetLastPrice(s, "diesel")
					if status != "open" {
						database.WritePrice(s, e5, e10, diesel, false)
					} else {
						b, ok := res.Prices[s].(map[string]any)["e5"].(bool)
						if !ok || b {
							e5 = res.Prices[s].(map[string]any)["e5"].(float64)
						}

						b, ok = res.Prices[s].(map[string]any)["e10"].(bool)
						if !ok || b {
							e10 = res.Prices[s].(map[string]any)["e10"].(float64)
						}

						b, ok = res.Prices[s].(map[string]any)["diesel"].(bool)
						if !ok || b {
							diesel = res.Prices[s].(map[string]any)["diesel"].(float64)
						}
						database.WritePrice(s, e5, e10, diesel, true)
					}
					log.Println("Updated", s)
				}
			}
			time.Sleep(45*time.Second + time.Duration(rand.Intn(30))*time.Second)
		}
		go SendNotifications()
	}
}

// Search Stations
func SearchStations(lat float64, long float64) []variables.Station {
	var stations []variables.Station
	resp, err := http.Get(fmt.Sprintf("https://creativecommons.tankerkoenig.de/json/list.php?lat=%f&lng=%f&rad=3&type=all&sort=dist&apikey=%s", lat, long, config.Data.Tank_api))
	if err != nil {
		log.Println(err)
	} else if resp.StatusCode != http.StatusOK {
		log.Println(errors.New("unexpected status" + resp.Status))
	} else {
		defer resp.Body.Close()

		var res variables.ApiStructOne

		json.NewDecoder(resp.Body).Decode(&res)

		for _, station := range res.Stations {
			database.AddStation(station.Id, station.Name+" | "+station.Street+" "+station.HouseNumber+", "+station.Place, station.Lat, station.Lng)
			database.WritePrice(station.Id, station.E5, station.E10, station.Diesel, station.IsOpen)
			stations = append(stations, variables.Station{Id: station.Id, Name: station.Name + " | " + station.Street + " " + station.HouseNumber + ", " + station.Place, Lat: station.Lat, Long: station.Lng})
		}
	}
	return stations
}

// This handler is called everytime telegram sends us a webhook event
func Handler(res http.ResponseWriter, req *http.Request) {
	p := req.URL.Path
	// Return list of all watched stations
	if p == "/stations" {
		json.NewEncoder(res).Encode(database.GetStations())
		// Handle Telegram hooks
	} else if p == "/" {
		// First, decode the JSON response body
		body := &webhookReqBody{}
		if err := json.NewDecoder(req.Body).Decode(body); err != nil {
			log.Println("could not decode request body: ", err, req.Body)
		} else {
			switch body.Message.Text {
			case "/start":
				buffer.Delete(body.Message.Chat.ID)
				go Start(body.Message.Chat.ID)
			case "/ziel":
				buffer.Delete(body.Message.Chat.ID)
				go SetTarget(body.Message.Chat.ID)
			case "/typ":
				buffer.Delete(body.Message.Chat.ID)
				go SetType(body.Message.Chat.ID)
			case "/aktuell":
				buffer.Delete(body.Message.Chat.ID)
				go SendCurrent(body.Message.Chat.ID)
			case "/statistik":
				buffer.Delete(body.Message.Chat.ID)
				go SendStats(body.Message.Chat.ID)
			case "/benachrichtigung":
				buffer.Delete(body.Message.Chat.ID)
				go UpdateNotification(body.Message.Chat.ID)
			default:
				buffer.Delete(body.Message.Chat.ID)
				// Handle Admin Notifications for all users
				if strings.HasPrefix(body.Message.Text, "/msg") {
					if slices.Contains(config.Data.Admin_id, body.Message.Chat.ID) {
						// trimming the prefix and then sending the message to all users, regardless if they have opted out of notifications
						msg := strings.TrimPrefix(body.Message.Text, "/msg")
						msg = strings.TrimSpace(msg)
						settings := database.ReadSettings()
						for _, set := range settings {
							SendMsg(set.Id, msg)
						}
					} else {
						SendMsg(body.Message.Chat.ID, "Nachricht wurde nicht erkannt.")
					}

				} else /*handle locations*/ if body.Message.Location.Lat == 0 && body.Message.Location.Long == 0 && body.Message.Location.Live != 0 && body.Message.Text == "" {
					stations := SearchStations(body.Edited_message.Location.Lat, body.Edited_message.Location.Long)
					database.SetStations(body.Message.Chat.ID, stations)
				} else if body.Message.Location.Lat != 0 && body.Message.Location.Long != 0 && body.Message.Text == "" {
					stations := SearchStations(body.Message.Location.Lat, body.Message.Location.Long)
					database.SetStations(body.Message.Chat.ID, stations)
					var msg string
					for _, s := range stations {
						if database.GetStationStatus(s.Id) {
							msg = msg + "*" + cases.Title(language.German).String(database.GetStationName(s.Id)) + "*\nE5:        " + strconv.FormatFloat(database.GetLastPrice(s.Id, "e5"), 'f', 3, 64) + " €\nE10:      " + strconv.FormatFloat(database.GetLastPrice(s.Id, "e10"), 'f', 3, 64) + " €\nDiesel:  " + strconv.FormatFloat(database.GetLastPrice(s.Id, "diesel"), 'f', 3, 64) + " €\n"
						} else {
							msg = msg + "*" + cases.Title(language.German).String(database.GetStationName(s.Id)) + "*\nGeschlossen\n"
						}
					}
					SendMsg(body.Message.Chat.ID, msg)
					// Filter everything unexpected
				} else if body.Message.Text == "E5" || body.Message.Text == "E10" || body.Message.Text == "Diesel" {
					buffer.Set(body.Message.Chat.ID, body.Message.Text)
				} else if body.Message.Text == "JA" || body.Message.Text == "NEIN" {
					buffer.Set(body.Message.Chat.ID, body.Message.Text)
				} else {
					_, err := strconv.ParseFloat(strings.ReplaceAll(strings.TrimSpace(body.Message.Text), ",", "."), 64)
					if err != nil {
						log.Println(err)
						SendMsg(body.Message.Chat.ID, "Nachricht wurde nicht erkannt.")
					} else {
						buffer.Set(body.Message.Chat.ID, body.Message.Text)
					}
				}
			}
		}

		// log a confirmation message if the message is sent successfully
		log.Println("reply sent")
	}
}
