package config

import (
	"encoding/json"
	"io"
	"log"
	"os"
)

var Data confData

type confData struct {
	Tank_api         string  `json:"tank_api"`
	Telegram_api_key string  `json:"telegram_api_key"`
	Influxdb_url     string  `json:"influxdb_url"`
	Influxdb_org     string  `json:"influxdb_org"`
	Influxdb_bucket  string  `json:"influxdb_bucket"`
	Influxdb_token   string  `json:"influxdb_token"`
	Admin_id         []int64 `json:"admin_id"`
}

func ReadFileAndUnmarshal(filePath string) error {
	file, err := os.Open(filePath)
	if err != nil {
		log.Fatal("Config file could not be loaded.", err.Error())
		return err
	}
	defer file.Close()
	data, err := io.ReadAll(file)
	if err != nil {
		log.Fatal("Could no read the config file.", err.Error())
		return err
	}
	err = json.Unmarshal(data, &Data)
	if err != nil {
		log.Fatal("Could not unmarshal the json.", err.Error())
		return err
	}
	return err
}
