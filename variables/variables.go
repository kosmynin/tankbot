package variables

import (
	"database/sql"
)

// a package that contains global config variables that do not change

var ConfigFileLocation string = "./config/config.json"

var DB *sql.DB

// API response for station search
type ApiStructOne struct {
	Ok       bool             `json:"ok"`
	License  string           `json:"license"`
	Data     string           `json:"data"`
	Status   string           `json:"status"`
	Stations []StationsStruct `json:"stations"`
}
type StationsStruct struct {
	Id          string  `json:"id"`
	Name        string  `json:"name"`
	Brand       string  `json:"brand"`
	Street      string  `json:"street"`
	Place       string  `json:"place"`
	Lat         float64 `json:"lat"`
	Lng         float64 `json:"lng"`
	Dist        float64 `json:"dist"`
	Diesel      float64 `json:"diesel"`
	E5          float64 `json:"e5"`
	E10         float64 `json:"e10"`
	IsOpen      bool    `json:"isOpen"`
	HouseNumber string  `json:"houseNumber"`
	PostCode    int     `json:"postCode"`
}

// API response for prices at specific station
type ApiStructTwo struct {
	Ok      bool           `json:"ok"`
	License string         `json:"license"`
	Data    string         `json:"data"`
	Prices  map[string]any `json:"prices"`
}

// Internal Station definition for sqlite
type Station struct {
	Id   string
	Name string
	Lat  float64
	Long float64
}

// Internal user setting definition for sqlite
type Settings struct {
	Id               int64
	Type             string
	Target           float64
	LastNotification int64
	Stations         string
	SendNotification int64
}
