package database

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"strings"
	"time"

	"gonum.org/v1/plot/plotter"

	influx "github.com/influxdata/influxdb-client-go/v2"
	"github.com/influxdata/influxdb-client-go/v2/api"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/kosmynin/TankBot/config"
	"gitlab.com/kosmynin/TankBot/variables"
)

var (
	influx_write api.WriteAPIBlocking
	influx_query api.QueryAPI
)

// Initialize sqlite and Influx DBs
func Init() {
	// initiate sql database
	var err error
	variables.DB, err = sql.Open("sqlite3", "./data.sqlite")
	if err != nil {
		log.Println(err)
		return
	}
	// set the max connection lifetime to three minutes
	variables.DB.SetConnMaxLifetime(time.Minute * 3)
	// sets the max simultaniosly open connections to ten
	variables.DB.SetMaxOpenConns(10)
	// sets the max number of idle connections to 10
	variables.DB.SetMaxIdleConns(10)
	_, err = variables.DB.Exec("Create table if not exists settings ( id INTEGER PRIMARY KEY, type STRING, target FLOAT, lastNotification INTEGER, stations STRING, sendNotification INT DEFAULT 1);")
	if err != nil {
		log.Println(err)
		return
	}
	_, err = variables.DB.Exec("Create table if not exists stations ( id STRING PRIMARY KEY, name STRING, lat FLOAT, long FLOAT);")
	if err != nil {
		log.Println(err)
		return
	}

	// initiate influx database
	// Create a new client using an InfluxDB server base URL and an authentication token
	influx_client := influx.NewClient(config.Data.Influxdb_url, config.Data.Influxdb_token)
	// Use blocking write client for writes to desired bucket
	influx_write = influx_client.WriteAPIBlocking(config.Data.Influxdb_org, config.Data.Influxdb_bucket)
	influx_query = influx_client.QueryAPI(config.Data.Influxdb_org)
	influx_write.EnableBatching()

}

// Get user config from sqlite
func ReadConfig(user int64) variables.Settings {
	var settings variables.Settings
	row := variables.DB.QueryRow("SELECT * FROM settings WHERE id = ?;", user)

	err := row.Scan(&settings)
	if err != nil {
		log.Println(err)
		return settings
	}

	return settings
}

// Get all users settings from sqlite
func ReadSettings() []variables.Settings {
	var settings []variables.Settings
	rows, err := variables.DB.Query("SELECT * FROM settings;")
	if err != nil {
		log.Println(err)
	}
	defer rows.Close()

	for rows.Next() {
		var set variables.Settings
		if err := rows.Scan(&set.Id, &set.Type, &set.Target, &set.LastNotification, &set.Stations, &set.SendNotification); err != nil {
			log.Println(err)
		}
		settings = append(settings, set)
	}
	return settings
}

// Set fuel type for user
func SetType(user int64, typ string) {
	_, err := variables.DB.Exec("UPDATE settings SET type = $1 WHERE id = $2;", typ, user)
	if err != nil {
		log.Println(err)
	}
}

// Set watch target for user
func SetTarget(user int64, target float64) {
	_, err := variables.DB.Exec("UPDATE settings SET target = $1 WHERE id = $2;", target, user)
	if err != nil {
		log.Println(err)
	}
}

// Update time of last notification
func UpdateTime(user int64) {
	_, err := variables.DB.Exec("UPDATE settings SET lastNotification = $1 WHERE id = $3;", time.Now().Unix(), user)
	if err != nil {
		log.Println(err)
	}
}

//Update if the user wants to receive notifications
func UpdateNotification(user int64, sendNotif int64) {
	_, err := variables.DB.Exec("UPDATE settings SET sendNotification=? WHERE id=?;",sendNotif, user)
	if err != nil{
		log.Println(err)
	}
}

// Set fuel stations associated with user
func SetStations(user int64, stations []variables.Station) {
	var stationsList []string
	for _, s := range stations {
		stationsList = append(stationsList, s.Id)
	}
	stationsStr := strings.Join(stationsList[:], ",")

	_, err := variables.DB.Exec("UPDATE settings SET stations = $1 WHERE id = $2;", stationsStr, user)
	if err != nil {
		log.Println(err)
		return
	}
}

// Get fuel stations associated with user
func GetSetStations(user int64) string {
	stations := ""
	row := variables.DB.QueryRow("SELECT stations from settings where id = ?", user)

	err := row.Scan(&stations)
	if err != nil {
		log.Println(err)
	}
	return stations
}

// Get fuel type for user
func GetType(user int64) string {
	typ := "e5"
	row := variables.DB.QueryRow("SELECT type from settings where id = ?", user)

	err := row.Scan(&typ)
	if err != nil {
		log.Println(err)
	}
	return typ
}

// Write complete user settings to sqlite
func WriteConfig(user int64, type_value string, price float64, lastNot int64, stations string) {
	_, err := variables.DB.Exec("INSERT OR IGNORE INTO settings ( id, type, target, lastNotification, stations) VALUES ($1, $2, $3, $4, $5);", user, type_value, price, lastNot, stations)
	if err != nil {
		log.Println(err)
	}
}

// Adds a fuel station to global station list
func AddStation(id string, name string, lat float64, long float64) {
	_, err := variables.DB.Exec("INSERT OR IGNORE INTO stations ( id, name, lat, long) VALUES ($1, $2, $3, $4);", id, name, lat, long)
	if err != nil {
		log.Println(err)
	}
}

// Get all saved stations
func GetStations() []variables.Station {
	var stations []variables.Station
	rows, err := variables.DB.Query("SELECT * FROM stations;")
	if err != nil {
		log.Println(err)
	}
	defer rows.Close()

	for rows.Next() {
		var station variables.Station
		if err := rows.Scan(&station.Id, &station.Name, &station.Lat, &station.Long); err != nil {
			log.Println(err)
		}
		stations = append(stations, station)
	}
	if err = rows.Err(); err != nil {
		log.Println(err)
	}

	return stations
}

// Get name of a fuel station
func GetStationName(id string) string {
	var station string
	row := variables.DB.QueryRow("SELECT name from stations where id = ?", id)

	err := row.Scan(&station)
	if err != nil {
		log.Println(err)
	}
	return station
}

// Write a Price to Influx
func WritePrice(station string, e5 float64, e10 float64, diesel float64, open bool) {
	p := influx.NewPointWithMeasurement(station).
		AddField("e5", e5).
		AddField("e10", e10).
		AddField("diesel", diesel).
		AddField("open", open).
		SetTime(time.Now())
	err := influx_write.WritePoint(context.Background(), p)

	if err != nil {
		log.Println(err)
	}
	influx_write.Flush(context.Background())

}

// Get last fuel Price for a station and type
func GetLastPrice(station string, type_value string) float64 {
	query := fmt.Sprintf(`from(bucket:"%s")|> range(start: -1d) |> filter(fn:(r) => r._measurement == "%s") |> filter(fn:(r) => r._field == "%s") |> filter(fn: (r) => r._value > 0) |> last()`, config.Data.Influxdb_bucket, station, type_value)
	// Get parser flux query result
	result, err := influx_query.Query(context.Background(), query)
	if err == nil {
		// Use Next() to iterate over query result lines
		for result.Next() {
			// read result
			return result.Record().Value().(float64)
		}
		if result.Err() != nil {
			fmt.Printf("Query error: %s\n", result.Err().Error())
		}
	} else {
		log.Println(err)
	}
	return 0
}

// Return whether a station is open
func GetStationStatus(station string) bool {
	query := fmt.Sprintf(`from(bucket:"%s")|> range(start: -1d) |> filter(fn:(r) => r._measurement == "%s") |> filter(fn:(r) => r._field == "open") |> last()`, config.Data.Influxdb_bucket, station)
	// Get parser flux query result
	result, err := influx_query.Query(context.Background(), query)
	if err == nil {
		// Use Next() to iterate over query result lines
		for result.Next() {
			// read result
			return result.Record().Value().(bool)
		}
		if result.Err() != nil {
			fmt.Printf("Query error: %s\n", result.Err().Error())
		}
	} else {
		log.Println(err)
	}
	return false
}

// Return whether a station is open
func GetLowest(station string, type_value string) float64 {
	query := fmt.Sprintf(`from(bucket:"%s")|> range(start: -10d) |> filter(fn:(r) => r._measurement == "%s") |> filter(fn:(r) => r._field == "%s") |> filter(fn: (r) => r._value > 0) |> quantile(q: 0.0001)`, config.Data.Influxdb_bucket, station, type_value)
	// Get parser flux query result
	result, err := influx_query.Query(context.Background(), query)
	if err == nil {
		// Use Next() to iterate over query result lines
		for result.Next() {
			// read result
			return result.Record().Value().(float64)
		}
		if result.Err() != nil {
			fmt.Printf("Query error: %s\n", result.Err().Error())
		}
	} else {
		log.Println(err)
	}
	return 0
}


// Get fuel prices from the last 10 days for a station and type
func GetLastWeeks(station string, type_value string) plotter.XYs {
	var datapoints plotter.XYs
	query := fmt.Sprintf(`from(bucket:"%s")|> range(start: -10d) |> filter(fn:(r) => r._measurement == "%s") |> filter(fn:(r) => r._field == "%s") |> filter(fn: (r) => r._value > 0)`, config.Data.Influxdb_bucket, station, type_value)
	// Get parser flux query result
	result, err := influx_query.Query(context.Background(), query)
	if err != nil {
		panic(err)
	} else {
		// Use Next() to iterate over query result lines
		for result.Next() {
			var dp plotter.XY
			// read result
			dp.X = float64(result.Record().Time().Unix())
			dp.Y = result.Record().Value().(float64)
			datapoints = append(datapoints, dp)
		}
		if result.Err() != nil {
			fmt.Printf("Query error: %s\n", result.Err().Error())
		}
	}

	return datapoints
}
