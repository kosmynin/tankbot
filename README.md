# TankBot

## Configuration
```json
{
    "tank_api":"<tankerkönig api key>",
    "telegram_api_key":"<telegram api key>",
    "influxdb_url":"<influxdb url>",
    "influxdb_org":"org",
    "influxdb_bucket":"tankbot",
    "influxdb_token":"<influxdb auth token>",
    "admin_id": <id of admin user>
}
```

## docker-compose
```yml
version: "3.8"
services:
  tankbot:
    image: registry.gitlab.com/kosmynin/tankbot:latest
    ports:
      - 8080:8080
    restart: unless-stopped
    volumes:
      - ./config.json:/app/config/config.json
      - ./data.sqlite:/app/data.sqlite
```
